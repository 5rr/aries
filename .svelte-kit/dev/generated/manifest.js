const c = [
	() => import("../../../src/routes/__layout.svelte"),
	() => import("../components/error.svelte"),
	() => import("../../../src/routes/index.svelte"),
	() => import("../../../src/routes/services.svelte"),
	() => import("../../../src/routes/contact.svelte"),
	() => import("../../../src/routes/about.svelte"),
	() => import("../../../src/routes/auth/password-reset.svelte"),
	() => import("../../../src/routes/auth/register.svelte"),
	() => import("../../../src/routes/auth/login.svelte"),
	() => import("../../../src/routes/blog.svelte"),
	() => import("../../../src/routes/test.svelte")
];

const d = decodeURIComponent;

export const routes = [
	// src/routes/index.svelte
	[/^\/$/, [c[0], c[2]], [c[1]]],

	// src/routes/services.svelte
	[/^\/services\/?$/, [c[0], c[3]], [c[1]]],

	// src/routes/contact.svelte
	[/^\/contact\/?$/, [c[0], c[4]], [c[1]]],

	// src/routes/about.svelte
	[/^\/about\/?$/, [c[0], c[5]], [c[1]]],

	// src/routes/auth/password-reset.svelte
	[/^\/auth\/password-reset\/?$/, [c[0], c[6]], [c[1]]],

	// src/routes/auth/register.svelte
	[/^\/auth\/register\/?$/, [c[0], c[7]], [c[1]]],

	// src/routes/auth/login.svelte
	[/^\/auth\/login\/?$/, [c[0], c[8]], [c[1]]],

	,

	// src/routes/blog.svelte
	[/^\/blog\/?$/, [c[0], c[9]], [c[1]]],

	// src/routes/test.svelte
	[/^\/test\/?$/, [c[0], c[10]], [c[1]]]
];

// we import the root layout/error components eagerly, so that
// connectivity errors after initialisation don't nuke the app
export const fallback = [c[0](), c[1]()];