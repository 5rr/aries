
export const setSessionCookie=(body)=>{
    document.cookie = `jwt=${window.btoa(
        JSON.stringify(body)
    )}; Path=/; HttpOnly SameSite=None Secure`;
}